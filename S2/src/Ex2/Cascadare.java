package Ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Cascadare extends JFrame {
    JLabel field1, field2;
    JTextField f1, f2;
    JButton b;

    void Frame() {
        setTitle("Apasa pe buton");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(500, 500);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);
        int width = 80;
        int height = 20;

        field1 = new JLabel("Text ");
        field1.setBounds(10, 50, width, height);

        field2 = new JLabel("Text ");
        field2.setBounds(10, 100, 200, height);


        f1 = new JTextField();
        f1.setBounds(150, 50, width, height);

        f2 = new JTextField();
        f2.setBounds(150, 100, width, height);

        b = new JButton("Click aici!");
        b.setBounds(10, 200, 100, height);
        b.addActionListener(new TratareButon());

        add(field1);
        add(field2);
        add(f1);
        add(f2);
        add(b);
    }

    public static void main(String[] args) {

        new Cascadare();
    }

    class TratareButon implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if (b == actionEvent.getSource()) {
                System.err.println("Test");

            }
        }
    }
}

